import React from "react"

import './css/portal.css'

export default class Portal extends React.Component{

    constructor(props, context) {
        super(props, context)

        this.state = {
        }
    }

    componentDidMount(){

    }

    actualList = () => {

        let arr = [
            {
                picURL: "http://www.thedesignfirm.in/img/logoo.png",
                name: 'TDF-THE DESIGN FIRM',
                website: 'www.thedesignfirm.in',
                details: '',
                lastDate: '07-05-2018',
                startDate: '',
                duration: '1 year',
                positionsOpen: '1-3',
                skillsRequired: 'Design and Drafting',
                whoCanApply: 'All Indian Universities Architecture students',
                perks: 'yes',
                stipend: '4000 - 6000',
                formURL : 'https://docs.google.com/forms/d/e/1FAIpQLScLkcLCHeUgh5AfTj0JQIKdkxAdFbH0ZKECyuGEPX0R01yN5A/viewform?usp=sf_link'
            },
            {
                picURL: "http://www.kembhaviarchitects.com/wp-content/uploads/2016/12/logo6_03.png",
                name: 'KAF-Kembhavi Architecture Foundation',
                website: 'www.kembhaviarchitects.com',
                details: 'NA',
                lastDate: '28-04-2018',
                startDate: 'NA',
                duration: '5 months',
                positionsOpen: '7',
                skillsRequired: 'AUTOCAD, PHOTOSHOP,3D MAX, SKETCH UP, REVIT',
                whoCanApply: '8th & 9th semester students',
                perks: 'yes',
                stipend: 'Company standards',
                formURL : 'https://docs.google.com/forms/d/e/1FAIpQLSfn7k9uJ1hwc85h9rIfE7exrxs7X6m34nelIIYEybGa03RWVg/viewform?usp=sf_link'
            },

            {
                picURL: "http://images.jdmagicbox.com/comp/bangalore/b5/080pxx80.xx80.110226123902.w4b5/catalogue/school-serv-india-solutions-pvt-ltd-mahadevapura-bangalore-educational-institution-set-up-consultants-1vspzwy.jpg",
                name: 'School Serv Solution Private Limited',
                website: 'http://www.schoolserv.in',
                details: 'Creating innovative designs and presentations and maintaining important official documents',
                lastDate: '2nd May',
                startDate: 'Between 25th April and 18th May',
                duration: '6 Months',
                positionsOpen: '2',
                skillsRequired: 'AUTOCAD',
                whoCanApply: '8th and 9th Semester students',
<<<<<<< HEAD
                perks: 'yes',
                stipend: 'NA',
                formURL : ''
            },
            
=======
                perks: 'Pre placement offer (PPO)',
                stipend: '12000',
                formURL : 'https://docs.google.com/forms/d/e/1FAIpQLSfxxi3f1_DXi67AezRitVM7t186RrREo9GxN3mgr4-E7UBdew/viewform?usp=sf_link'
            },
            {
                  picURL: "https://is1-ssl.mzstatic.com/image/thumb/Purple118/v4/61/c3/0d/61c30da7-102e-c2cd-15c4-1768a3f31b20/source/512x512bb.jpg",
                  name: 'Appidoc Healthcare Private Ltd',
                  website: 'https://www.appidoc.com/',

                  details: `Following through design development drawings and collaborate with the production staff for technical details and completion of construction drawings,Designing schematics specialised in space programming and site planning as well as developing of elevations, sections, 3D renderings and architectural details of the project, Assist in construction drawings of pre-fabricated structure projects and assist in the streamlining of the architectural design process through overall completion of the project`,
                  lastDate: '11th May',
                  startDate: 'Between 25th April and 11th May',
                  duration: '6 Months',
                  positionsOpen: '1',
                  skillsRequired: 'AUTOCAD and MS Office',
                  whoCanApply: '8th and 9th Semester students',
                  perks: 'Certificate, Letter of recommendation, Pre placement offer (PPO), Flexible work hours, Informal dress code, 5 days a week',
                  stipend: '10000',
                  formURL : 'https://docs.google.com/forms/d/e/1FAIpQLSc9Px9Okgj9GuICRdgCSg6L8vqTIzwddllEC-z4ziFbzvJZxA/viewform?usp=sf_link'
              },
              {
                  picURL: "https://madeinearth.in/wp-content/uploads/2017/09/cropped-MIE-only_white.jpg",
                  name: 'Made in Earth',
                  website: 'https://madeinearth.in/',
                  details: 'Currently hiring for Internship - Mail details at apply@madeinearth.in along with portfolio link uploaded in Archibuddy',
                  lastDate: 'NA',
                  startDate: 'NA',
                  duration: 'NA',
                  positionsOpen: '',
                  skillsRequired: 'Design and Drafting',
                  whoCanApply: '8th and 9th Semester students',
                  perks: 'yes',
                  stipend: '',
                  formURL : ''
              },
>>>>>>> c6275eeb572f7144553e88a8cb3c15702e8ba6e8
        ]

        return (

            arr.map((item, i) => (
                <div className="list">
                        <div className="pic"><img src={item.picURL} alt=""/></div>
                        <div className="name pad"><p><strong>Name of the firm </strong> {item.name}</p></div>
                        <div className="website pad"><p><strong>Website </strong> {item.website}</p></div>
                        <div className="details pad"><p><strong>Details of firm </strong>  {item.details}</p></div>
                        <div className="lastDate pad"><p><strong>Last date of applying </strong>  {item.lastDate}</p></div>
                        <div className="startDate pad"><p><strong>Internship start date </strong>  {item.startDate}</p></div>
                        <div className="duration pad"><p><strong>Duration </strong>  {item.duration}</p></div>
                        <div className="positionsOpen pad"><p><strong>Open positions </strong>  {item.positionsOpen}</p></div>
                        <div className="skillsRequired pad"><p><strong>Skills required </strong>  {item.skillsRequired}</p></div>
                        <div className="whoCanApply pad"><p><strong>Who can apply </strong>  {item.whoCanApply}</p></div>
                        <div className="perks pad"><p><strong>Perks offered</strong>  {item.perks}</p></div>
                        <div className="stipend pad"><p><strong>Stipend </strong>  {item.stipend}</p></div>
                        <div
                        className="applyBtn"
                        onClick = {() => window.open(item.formURL, '_self')}

                        ><p>Apply</p></div>
            </div>
            ))

        )
    }


    render(){
        return (
            <div className="archiLogo">
                <img src="https://s3.amazonaws.com/xi-upload/logo-01.svg" alt=""/>
                <h1>Archibuddy Internship portal.</h1>
                <p>Please click on the apply button on the desired firm</p>
                <p>Note: Upload your projects on <a href="http://archibuddy.com"  style={{cursor: "pointer"}}>archibuddy.com</a>  and then start applying.</p>
                <div className="outerWrapper">
                <div className="listWrapper">
                    {this.actualList()}
                </div>
                </div>





            </div>

        )
    }
}
