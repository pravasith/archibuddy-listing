import React, { Component } from 'react';
import Portal  from './portal'
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Portal/>
      </div>
    );
  }
}

export default App;
